//
//  Job.hpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

using namespace std;
#pragma once
#include <string>
#include "Address.hpp"
#include "Contact.hpp"
#include "Date.hpp"

class Job {
    string companyName, position;
    Address companyAddress;
    Contact workContact;
    Date hireDate;
    
public:
    Job();
    Job(string companyName, string postion, Address companyAddress, Contact workContact, Date hireDate);
    
    void setCompanyName(string);
    void setPosition(string);
    void setCompanyAddress(Address);
    void setWorkConatact(Contact);
    void setHireDate(Date);
    void setWorkEmail(string);
    
    
    
    string getCompanyName();
  
    string getPosition();
    
    Address getCompanyAddress();
    
    Contact getWorkContact();
    
    Date getHireDate();
    int getHireYear();
    
    void display();
};

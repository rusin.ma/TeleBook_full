//
//  Person.hpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//


#pragma once
#include "Date.hpp"
#include <string>
using namespace std;

class Person : public Date {
    string name, surname;
    int day, month, year;
   
public:
    Person();
    Person(string name, string surname, int day, int month, int year);
    
    void setName(string);
    void setSurname(string);

    
    string getName();
    string getSurname();

    void display();
    
};

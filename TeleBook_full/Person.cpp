//
//  Person.cpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#include "Person.hpp"
#include <iostream>

Person::Person():name("Jan"), surname("Kowalski"){}


Person::Person(string name, string surname, int day, int month, int year): name(name), surname(surname), Date(day, month, year) {}

void Person::setName(string name)
{
    this->name=name;
}

void Person::setSurname(string surname) {
    this->surname=surname;
}

string Person::getName() {
    return name;
}

string Person::getSurname() {
    return surname;
}

void Person::display()
{
    cout << "Imie i Nazwisko: " <<this->name <<" " << this->surname << endl;
    cout << "Data urodzenia: ";
    cout << getDay() <<"-" << getMonth() << "-" << getYear() <<endl;
   
}

//
//  Contact.cpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#include "Contact.hpp"
#include <iostream>

Contact::Contact(): phoneNumber("xxxxxx"), email("xxxx@gmail.com"){}
Contact::Contact(string phoneNumber, string email): phoneNumber(phoneNumber), email(email) {}

void Contact::setPhoneNumber(string phoneNumber)
{
    this->phoneNumber=phoneNumber;
}

void Contact::setEmail(string email)
{
    this->email=email;
}

string Contact::getEmail()
{
    return email;
}

string Contact::getPhoneNumber()
{
    return phoneNumber;
}

void Contact::display()
{
    cout << "Kontakt: " << this->phoneNumber << " " << this->email <<endl;
}

//
//  Address.hpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//


#pragma once
#include <string>
using namespace std;

class Address {
    string street, city;
    int houseNumber, zipCode;
    
public:
    Address();
    Address(string street, int houseNumber, string city, int zipCode);
    
    void setStreet(string);
    void setHouseNumber(int);
    void setCity(string);
    void setZipCode(int);
    
    string getStreet();
    int getHouseNumber();
    string getCity();
    int getZipCode();
    
    void display();
    
};

//
//  Contact.hpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#pragma once
#include <string>
using namespace std;

class Contact {
    string phoneNumber, email;
    
public:
    Contact();
    Contact(string phoneNumber, string email);
    
    void setPhoneNumber(string);
    void setEmail(string);
    
    string getPhoneNumber();
    string getEmail();
    
    void display();
};

//
//  Address.cpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#include "Address.hpp"
#include <iostream>

Address::Address():street("Ulica"), houseNumber(0), city("Miasto"), zipCode(00000) {}
Address::Address(string street, int houseNumber, string city, int zipCode):street(street), houseNumber(houseNumber), city(city), zipCode(zipCode) {}

void Address::setStreet(string street)
{
    this->street = street;
}

void Address::setHouseNumber(int number)
{
    this->houseNumber = number;
}

void Address::setCity(string city) {
    this->city = city;
}

void Address::setZipCode(int zipCode) {
    this->zipCode=zipCode;
}

string Address::getCity() {
    return city;
}

string Address::getStreet() {
    return street;
}

int Address::getZipCode() {
    return zipCode;
}

int Address::getHouseNumber(){
    return houseNumber;
}

void Address::display(){
    cout<< this->street << " " << this->houseNumber << endl;
    cout<< this->city << " " << this->zipCode<<endl;
}






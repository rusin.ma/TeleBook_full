//
//  Date.cpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#include "Date.hpp"
using namespace std;
#include<iostream>

Date::Date(): day(1), month(1), year(1900) {
    
}
Date::Date(int day, int month, int year): day(day), month(month), year(year)
{
    while (month <=0 || month >12) {
        cout << "Błędny miesiąc, podaj poprawny: "<<endl;
        cin >> month;
        this->month = month;
    }
}

void Date::setDay(int day)
{
    this->day=day;
}

void Date::setYear(int year)
{
    this->year = year;
}

void Date::setMonth(int month)
{
    this->month = month;
}

int Date::getDay()
{
    return day;
}

int Date::getMonth()
{
    return month;
}

int Date::getYear()
{
    return year;
}

void Date::display(){
    cout << "Dzien - miesiac - rok: " <<this->day << "-" << this->month << "-" << this->year << endl;
}

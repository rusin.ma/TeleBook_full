//
//  Telebook.hpp
//  TeleBook_full
//
//  Created by Marta Rusin on 13/12/2022.
//

#pragma once
#include "Person.hpp"
#include "Contact.hpp"
#include "Address.hpp"
#include "Job.hpp"

class Telebook {
    Person person; Contact contact; Address address; Job job;
    
public:
    Telebook();
    Telebook(Person person, Contact homeContact, Address homeAddress, Job job);

    void setPerson(Person);
    void setContact(Contact);
    void setAddress(Address);
    void setJob(Job);
    void setSurName(string);
    
    void getPerson();
    void getContact();
    void getAddress();
    void getJob();
    void getPosition();
    
    void display();
};

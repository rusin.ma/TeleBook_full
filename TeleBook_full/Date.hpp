//
//  Date.hpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#pragma once

class Date {
protected:
    int day, month, year;
    
public:
    Date();
    Date(int day, int month, int year);
    
    void setDay(int);
    void setMonth(int);
    void setYear(int);
    
    int getDay();
    int getMonth();
    int getYear();
    
    void display();
    
};

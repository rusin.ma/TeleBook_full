//
//  main.cpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#include <iostream>
using namespace std;

#include "Telebook.hpp"

int main(int argc, const char * argv[]) {

    
    Address companyAddress("Grunwaldzka", 100, "Gdansk", 80155);
    Job job_withDetails("Kainos", "Tester", companyAddress, Contact("334553", "eda@dada.com"), Date(1,15,2020));
    job_withDetails.display();

    cout << " ----------------------------" <<endl;

    Job job;
    job.display();

    cout << " ----------------------------" <<endl;

    cout << "Podaj nazwę firmy: " <<endl;
    string companyName;
    cin >> companyName;
    job.setCompanyName(companyName);

    cout <<"Podaj rok zatrudnienia: " << endl;
    int hireYear;
    cin>> hireYear;
    job.setHireDate(Date(job.getHireDate().getDay(), job.getHireDate().getMonth(), hireYear));

    cout << "Podaj email: " << endl;
    string email;
    cin >> email;
    job.setWorkEmail(email);

    job.display();

    cout <<"Display Hire Year: " << job.getHireYear() <<endl;
    
    
    Telebook empty;
    empty.display();
    
    return 0;
}

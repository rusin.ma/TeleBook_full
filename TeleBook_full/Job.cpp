//
//  Job.cpp
//  TeleBook_full
//
//  Created by Marta Rusin on 12/12/2022.
//

#include "Job.hpp"
#include<iostream>

Job::Job():companyName("Firma"), position("Stanowisko"){}
Job::Job(string companyName, string position, Address companyAddress, Contact workContact, Date hireDate):
companyName(companyName), position(position), companyAddress(companyAddress), workContact(workContact), hireDate(hireDate) {}

void Job::setCompanyName(string companyName){
    this->companyName=companyName;
}

void Job::setPosition(string position){
    this->position=position;
}

void Job::setHireDate(Date hireDate){
    this->hireDate=hireDate;
}

void Job::setWorkConatact(Contact workContact){
    this->workContact = workContact;
}

void Job::setWorkEmail(string email){
    this->workContact.setEmail(email);
}

void Job::setCompanyAddress(Address companyAddress){
    this->companyAddress=companyAddress;
}

string Job::getPosition() {
    return position;
}

string Job::getCompanyName(){
    return companyName;
}

Date Job::getHireDate() {
    return hireDate;
}

int Job::getHireYear() {
    return hireDate.getYear();
}

Contact Job::getWorkContact() {
    return workContact;
}

Address Job::getCompanyAddress(){
    return companyAddress;
}

void Job::display() {
    cout <<"Praca: " << endl;
    cout << "Firma: " << this->companyName << endl;
    cout << "Stanowisko: " << this->position << endl;
    cout << "Adres firmy: ";
    this->companyAddress.display();
    cout << "Dane kontaktowe praca: ";
    this->workContact.display();
    cout << "Data zatrudnienia: ";
    this->hireDate.display();
}
